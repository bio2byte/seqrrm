import os, os.path
import json
import argparse
import numpy as np
import pandas as pd
from Bio import SeqIO
import matplotlib.pyplot as plt
from Bio.Blast import NCBIWWW, NCBIXML
from b2bTools.msaBased.Predictor import MineSuiteMSA

# Input files parser
parser = argparse.ArgumentParser(description='Input handler')

parser.add_argument('-fasta', type=str,
					help='The input protein sequence/s in fasta format'
						 ' to run qBLAST and the biophysical predictions')

parser.add_argument('-align', type=str, nargs=2,
					help='MSA files to be aligned and biophysically compared')

args = parser.parse_args()


###############################################################################
###############################################################################

class BlastManager:
	def __init__(self, seq):
		self.rrm_seq = seq
		self.target_seq = []
		with open(self.rrm_seq, 'r') as f:
			for rec in SeqIO.parse(f, 'fasta'):
				self.target_seq = [res for res in rec.seq]
				break  # If the fasta file provided have more than one seq it
				# will only process the first one

		self.target_seq = ''.join(self.target_seq)
		self.file_name = str(
			input('Write the output filename for the BLAST results'
				  ' (String without extension nor spaces):\n'
				  '>>>') + '_blast.xml')
		self.mutated_seq = self.mutator(self.target_seq)

	def mutator(self, prot_seq):
		# 	Mutagenicity test, mutate the target sequence and add it
		mut_pos = int(
			input('Select one position in the sequence to mutate:\n' +
				  prot_seq + '\n>>>'))

		print('You have selected {}{} to mutate'.format(
			prot_seq[mut_pos], mut_pos))
		mut_res = str(input('Select the residue to mutate to:\n' +
							'A, C, D, E, F, G, H, I, K, L, M, N, P, Q, '
							'R, S, T, V, W, Y' +
							'\n>>>'))

		self.mutation = prot_seq[mut_pos] + str(mut_pos) + mut_res

		self.mutated_seq = list(prot_seq)
		self.mutated_seq[mut_pos] = mut_res
		return(self.mutated_seq)

	# Function to run the online-based version of BLAST (qblast) and generate
	# the output xml file
	def run_qblast(self, blast_prog='blastp', db='refseq_protein'):
		fasta_string = open(self.rrm_seq).read()
		result_handle = NCBIWWW.qblast(blast_prog, db, fasta_string,
									   format_type='XML')

		with open("{}".format(self.file_name), "w") as out_handle:
			out_handle.write(result_handle.read())

		self.parse_blast(self.file_name)

	# Parse the xml file with blast results generated in te function above
	def parse_blast(self, file_name):
		print('Running BLAST')
		# Debug line to input the xml file manually instead of running qblast
		result_handle = open(file_name)
		blast_record = NCBIXML.read(result_handle)

		# Define the column names that we wanna have in the final table
		column_names = ["accesion_version", "sequence_aligned",
						"alignment_length", "identity", "coverage", "e_value"]
		df = pd.DataFrame(columns=column_names)

		# Parse the aligment file by calculating the coverage and identities
		# and filter the results according to those and the e-value as well
		self.filepath = 'data_test_cases/{}.txt'.format(
			self.file_name.split('.')[0])

		with open(self.filepath, 'w+') as the_file:
			# If the file is empty append first the target & mutated sequence
			if os.stat(self.filepath).st_size == 0:
				the_file.write('>target_sequence\n')
				the_file.write(self.target_seq + '\n')
				the_file.write('>mutated_sequence\n')
				the_file.write(''.join(self.mutated_seq) + '\n')

			# Keep blast results with ID > 0.9, cov > 0.9, e-value < 0.04
			for alignment in blast_record.alignments:
				for hsp in alignment.hsps:
					coverage = hsp.align_length / blast_record.query_length
					identity = hsp.identities / hsp.align_length
					if identity > 0.9 and coverage > 0.9 and hsp.expect < 0.04:
						acc_version = alignment.title.split("|")[1]
						the_file.write('>{}\n'.format(acc_version))
						the_file.write(
							'{}'.format(hsp.sbjct.replace('-', '')) +
							'\n')

		# Run the MSA using all the BLAST matches found
		self.aligned_file = '{}_tcof.aln'.format(self.file_name.split('.')[0])
		cmd = "t_coffee {} -output=fasta_aln -outfile={}".format(
			self.filepath, self.aligned_file)
		os.system(cmd)

###############################################################################
###############################################################################

class MsaManager:
	def __init__(self):
		print('Running MsaManager')

	# Function that uses t-coffee software to align two MSAs
	def t_coffe_MSA_aligner(self, msa_files):
		if os.path.exists('msa_tmp.aln'):
			os.remove('msa_tmp.aln')
		self.out_file_name = 'msa_tmp.aln'
		self.msa_dict = {}

		for file in msa_files:
			filename = file.split('/')[-1].split('.')[0]
			self.msa_dict[filename] = []
			f = open(file, "r")
			self.msa_dict[filename] = [l.split('>')[1].split('\n')[0] for
									   l in f if l.startswith('>')]

		msa_files_tcof = ','.join(msa_files)
		cmd = "t_coffee -profile={} -outfile={}".format(msa_files_tcof,
														self.out_file_name)
		os.system(cmd)

###############################################################################
###############################################################################

class predManager:
	def __init__(self):
		print("Biophysical predictor class running...")

	# Simple function to get a list out of a dictionary keys
	def getList(self, dict):
		list = []
		for key in dict.keys():
			list.append(key)
		return list

	# Function to run all the predictors over an MSA file:
	# DynaMine, EFoldMine and Disomine)
	def run_predictors(self, msa_file, msa_map = None, mutation = None):
		# msa_map is a dictionary containing the original alignment with
		# file name (key) and sequence names (value), required when an
		# alignment of 2 MSAs is provided (None by default)
		ms = MineSuiteMSA()
		ms.predictSeqsFromMSA(msa_file)

		# Same but now mapped to full MSA, gaps are None
		ms.predictAndMapSeqsFromMSA(msa_file)

		self.mutated_seq_preds = None
		if args.fasta:
			self.target_seq_preds = ms.allAlignedPredictions['target_sequence']
			self.mutated_seq_preds = ms.allAlignedPredictions[
				'mutated_sequence']

		# Same but now mapped to reference sequence ID in MSA, gaps are None
		ms.predictAndMapSeqsFromMSA(msa_file, dataRead=True)
		ms.allAlignedPredictions_full = ms.allAlignedPredictions

		json_file_names = []

		# Create the results folder if it's not there yet
		if os.path.isdir("/results") == False:
			os.system("mkdir results")

		# When we work with an alignment of MSAs
		if msa_map != None:
			for key in msa_map.keys():
				# The two variables below are updated to include only the
				# sequences from one of the source MSA files at a time
				ms.allAlignedPredictions = {
					sel_key: ms.allAlignedPredictions_full[sel_key] for sel_key
					in msa_map[key]}
				ms.allSeqIds = self.getList(ms.allAlignedPredictions)

				# Same but now mapped to full MSA, distributions of scores.
				ms.getDistributions()
				msa_list = list(msa_map.keys())
				msa_list.remove(key)
				outfile_name = 'results/' \
							   '{}_aligned_to_{}_preds_distrib.json'.format(
								key, msa_list[0])

				with open(outfile_name, 'w') as fp:
					json.dump(ms.alignedPredictionDistribs, fp)
					json_file_names.append(outfile_name)

		# When we work with a single MSA
		else:
			# Same but now mapped to full MSA, distributions of scores.
			ms.getDistributions()

			outfile_name = 'results/' + msa_file.split(
				'/')[-1].split('.')[0] + '_preds_distrib.json'
			with open(outfile_name, 'w') as fp:
				json.dump(ms.alignedPredictionDistribs, fp)
				json_file_names.append(outfile_name)

		# Call the function below to plot the results
		self.plot_results(json_file_names, mutation)

	def plot_results(self, json_file_names, mutation):
		print(mutation)
		colors = ['blue', 'orange']

		num = int(input('Select the biophysical property to plot:\n'
						'1: Backbone dynamics\n'
						'2: Sidechain dynamics\n'
						'3: ppII\n'
						'4: Coil propensity\n'
						'5: Sheet propensity\n'
						'6: Helix propensity\n'
						'7: Early Folding\n'
						'8: DisoMine\n'
				  '(Enter the corresponding number)>>>'))


		biophys_dict = {1:'backbone', 2:'sidechain', 3: 'ppII', 4: 'coil',
						5: 'sheet', 6: 'helix', 7: 'earlyFolding',
						8: 'disoMine'}

		biophys_data =  biophys_dict[num]

		for file, col in zip(json_file_names, colors):
			with open(file) as json_file:
				data = json.load(json_file)
				none_idx = []
				# These double for loop got too complicated, I have to think
				# something simpler to handle the None values in the data
				for n in range(len(data[biophys_data]['median'])):
					if data[biophys_data]['median'][n] == None \
							or data[biophys_data]['firstQuartile'][n] == None \
							or data[biophys_data]['thirdQuartile'][n] == None:
						none_idx.append(n)

				range_list = []
				for n in range(len(none_idx)):
					try:
						if none_idx[n] + 1 != none_idx[n + 1]:
							range_list.append(
								(none_idx[n] + 1, none_idx[n + 1]))
						else:
							continue
					except:
						if len(none_idx) == 1:
							range_list.append((0, none_idx[0]))
							range_list.append((none_idx[0] + 1, len(
								data[biophys_data]['median'])))

						else:
							range_list.append((0, none_idx[0]))
							range_list.append((none_idx[-1] + 1, len(
								data[biophys_data]['median'])))

				# When there are None values in the data
				if range_list:
					for tuple in range_list:
						x = np.arange(tuple[0], tuple[1], 1)
						firstq = \
							data[biophys_data]['firstQuartile'][
							tuple[0]:tuple[1]]
						thirdq = \
							data[biophys_data]['thirdQuartile'][
							tuple[0]:tuple[1]]
						bottom = \
							data[biophys_data]['bottomOutlier'][
							tuple[0]:tuple[1]]
						top = \
							data[biophys_data]['topOutlier'][tuple[0]:tuple[1]]
						plt.fill_between(
							x, firstq, thirdq, alpha=0.5, color=col)
						plt.fill_between(
							x, bottom, top, alpha=0.25, color=col)

				# When there aren't None values in the data
				else:
					x = np.arange(0, len(data[biophys_data]['median']), 1)
					firstq = data[biophys_data]['firstQuartile']
					thirdq = data[biophys_data]['thirdQuartile']
					bottom = data[biophys_data]['bottomOutlier']
					top = data[biophys_data]['topOutlier']
					plt.fill_between(
						x, firstq, thirdq, alpha=0.5, color=col)
					plt.fill_between(
						x, bottom, top, alpha=0.25, color=col)

				plt.plot(data[biophys_data]['median'],
						 linewidth=1, color=col,
						 label=file.split('/')[-1].split('_')[0])

				if self.mutated_seq_preds:
					plt.plot(self.mutated_seq_preds[biophys_data],
							 linewidth=0.5, color='red',
							 label= mutation)

		plt.axis([0, len(data[biophys_data]['median']), 0, 1.1])
		plt.ylabel(biophys_data)
		plt.xlabel('Residue position')
		plt.legend(bbox_to_anchor=(1.0, 1.15))
		plt.show()
		plt.close()

###############################################################################
###############################################################################

if __name__ == '__main__':

	# Run blastp with fasta file as input
	if args.fasta:
		RRM = BlastManager(args.fasta)
		RRM.run_qblast()

		PRED = predManager()
		PRED.run_predictors(RRM.aligned_file, mutation = RRM.mutation)

	# Align two MSA files, run the predictors and plot the mapped results
	elif args.align:
		MSA = MsaManager()
		MSA.t_coffe_MSA_aligner(args.align)
		PRED = predManager()
		PRED.run_predictors(MSA.out_file_name, msa_map = MSA.msa_dict)

	else:
		print("No argument provided or invalid")

# from b2bTools.singleSeq.DynaMine.Predictor import DynaMine
# from b2bTools.singleSeq.EFoldMine.Predictor import EFoldMine
# from b2bTools.singleSeq.Predictor import MineSuite
#
# import time, sys

# if __name__ == '__main__':
#
# 	seqs = (('A','MEISRLAQSKRNIISLNMDLERDTQRIDEANQKLLLKIQEREDKIQRLESEIIQTRGLVEDEEWEKENRTTMERERALQELEEETARLERKNKTLVHSITELQQKLTRKSQKITNCEQSSPDGALEETKVKLQQLEASYACQEKELLKVMKEYAFVTQLCEDQALYIKKYQETLKKIEEELEALFLEREVSKLVSMNPVEKEHTSQNNEGTPTQKTARLFSKKIFCCLFFITLFFIRLLSYMFFHVRFINPDLLVNVLPKVLGRSTLWKLRCFFFPSLTLETEDMLPH'),
# 			('B','MPEVSSKGATISKKGFKKAVVKTQKKEGKKRKRTRKESYSIYIYKVLKQVHPDTGISSKAMSIMNSFVTDIFERIASEASRLAHYSKRSTISSREIQTAVRLLLPGELAKHAVSEGTKAVTKYTSSK'),
# 			('C','MDPKDRKKIQFSVPAPPSQLDPRQVEMIRRRRPTPAMLFRLSEHSSPEEEASPHQRASGEGHHLKSKRPNPCAYTPPSLKAVQRIAESHLQSISNLNENQASEEEDELGELRELGYPREEDEEEEEDDEEEEEEEDSQAEVLKVIRQSAGQKTTCGQGLEGPWERPPPLDESERDGGSEDQVEDPALSEPGEEPQRPSPSEPGT'),
# 			('adsf_%qut@**','MEEPQSDPSVEPPLSQETFSDLWKLLPENNVLSPLPSQAMDDLMLSPDDIEQWFTEDPGPDEAPRMPEAAPPVAPAPAAPTPAAPAPAPSWPLSSSVPSQKTYQGSYGFRLGFLHSGTAKSVTCTYSPALNKMFCQLAKTCPVQLWVDSTPPPGTRVRAMAIYKQSQHMTEVVRRCPHHERCSDSDGLAPPQHLIRVEGNLRVEYLDDRNTFRHSVVVPYEPPEVGSDCTTIHYNYMCNSSCMGGMNRRPILTIITLEDSSGNLLGRNSFEVRVCACPGRDRRTEEENLRKKGEPHHELPPGSTKRALPNNTSSSPQPKKKPLDGEYFTLQIRGRERFEMFRELNEALELKDAQAGKEPGGSRAHSSHLKSKKGQSTSRHKKLMFKTEGPDSD'))
#
# 	startTime = time.time()
# 	dm = DynaMine(predictionTypes=['backbone','helix'])
# 	dm.predictSeqs(seqs)
#
# 	print((dm.getAllPredictionsJson('test')))
#
# 	dm.writeAllPredictions('test','output/dynaMineResults/')
# 	print((time.time() - startTime))
#
# 	print((dm.allPredictions))
# 	print()
#
# 	startTime = time.time()
# 	dm = DynaMine()
# 	dm.predictSeqs(seqs)
# 	dm.writePredictionFile('output/testCoil.out','coil')
# 	print((time.time() - startTime))
#
# 	print((dm.allPredictions))
# 	print()
#
# 	startTime = time.time()
# 	efm = EFoldMine()
# 	efm.predictSeqs(seqs)
# 	print((time.time() - startTime))
#
# 	print((list(efm.allPredictions.keys())))
# 	print((efm.allPredictions['B']['earlyFolding']))
# 	print()
#
# 	startTime = time.time()
# 	efm = EFoldMine()
# 	efm.predictSeqs(seqs,dynaMinePreds=dm.allPredictions)
#
# 	efm.writePredictionFile('output/testBackbone.out','backbone')
# 	efm.writePredictionFile('output/testEarlyFolding.out','earlyFolding')
# 	print((time.time() - startTime))
#
# 	print((list(efm.allPredictions.keys())))
# 	print((efm.allPredictions['B']['earlyFolding']))
# 	print()
#
# 	ms = MineSuite()
# 	ms.predictSeqs(seqs)



