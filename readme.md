# seqRRM

_Software to perform biophysical predictions over an MSA, there are two main approaches:_

_1) Provide a fasta sequence of the target protein, generate a mutation through the terminal, run qBLAST against the refseq protein database, and finally plot the results of the biophysical prediction you want._

_2) Select two MSAs (The seed alignments for all the RRM pfam families are on rrm_pfam_data/, align the MSAs and then plot the biophysical predictions you want to compare them._

## Requirements & Installation

_python==3.7_

_T-COFFEE==13.39.0_

```
conda config --add channels bioconda
conda install t_coffee
```

_pandas==1.0.1_
```
conda install -c anaconda pandas
```
_biopython==1.76_
```
conda install -c anaconda biopython=1.76
```
_matplotlib==3.1.3_
```
conda install -c conda-forge matplotlib 
```

_Bio2Byte tools_
```
pip install b2bTools-2.0.3-py3-none-any.whl
```
All the packages are suggested to be installed through conda except
for the last one


## Run

_To run this software you just need to call the main.py script and provide the required arguments depending on the approach you want to follow._

_1) Run qBLAST: Only one argument in fasta format is accepted:_
```
python main.py -fasta your_protein_seq.fasta
```
You can find some fasta files under the data_test_cases folder. Consider that this calculation may take longer depending on the queues at the NCBI qBLAST server.

_2) Compare two MSAs: Only two arguments are accepted in the common alignment formats:_
```
python main.py -align first_msa.txt second_msa.txt
```
The RRM families alignments are under the rrm_pfam_data/rrm_pfam_seed_alignments/ folder


## How to cite

Roca, Joel, & Vranken, Wim. (2020). D2.1 Tool for interpretation of RRMs in the biophysical space. Zenodo. DOI: 10.5281/zenodo.4434534


## Funding

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No. 813239.

